FROM python:3.11-slim-buster
WORKDIR /code
COPY . .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["run:main.py", "0.0.0.0:5000"]
